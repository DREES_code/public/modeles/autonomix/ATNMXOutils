# Copyright (C) 2021. Logiciel élaboré par l'État, via la Drees.

# Pole Simulation du bureau Handicap et Dépendance, Drees.

# Ce programme informatique a été développé par la Drees. Il permet d'exécuter le modèle de microsimulation Autonomix version 1 de décembre 2021.

# Ce programme a été exécuté le 10/12/2021 avec la version 4.0.5 de R et les packages mobilisés sont listés dans le fichier DESCRIPTION du projet »

# La documentation du modèle peut être consultése sur gitlab : https://drees_code.gitlab.io/public/modeles/autonomix/DocumentationATNMX/

# Ce programme utilise les données de la base CARE Ménage 2015, ou des RI 2017 ou des RI 2017 anonymisées dans leur version du <date de la version utilisée>.

# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr

# Ce logiciel est régi par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/ et reproduite dans le fichier LICENCE diffusé conjointement au présent programme.
# En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.


gen_evol_ressources <- function(annee_reference) {
  #' gen_evol_ressources
  #'
  #' Lit les tables d'évolution des ressources par décile en montant et en taux.
  #' Transforme le taux en taux cumulé et retourne, pour CARE et pour RI, une
  #' table où chaque ligne représente une année, et chaque colonne, un taux ou un
  #' montant d'évolution des ressources pour un décile donné.
  #' Pour RI et CARE-I, l'année de départ est l'année 2016. Pour CARE-M, c'est 2014.
  #'
  #' @param
  #'
  #' @export
  #'
  #' @importFrom dplyr filter

  # On ne calcule une évolution des ressources que sur les années strictement postérieures à l'année de référence.
  evolution_ressources <- ATNMXOutils::evolution_ressources %>%
    mutate(
      evo_ress_dec1 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec1
        ),
      evo_ress_dec2 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec2
        ),
      evo_ress_dec3 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec3
        ),
      evo_ress_dec4 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec4
        ),
      evo_ress_dec5 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec5
        ),
      evo_ress_dec6 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec6
        ),
      evo_ress_dec7 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec7
        ),
      evo_ress_dec8 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec8
        ),
      evo_ress_dec9 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec9
        ),
      evo_ress_dec10 = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_ress_dec10
        ),
      evo_pension = case_when(
        annee <= annee_reference ~ 1,
        TRUE ~ evo_pension
        )
    )
  evolution_ressources[c(-1)] <- lapply(evolution_ressources[c(-1)], cumprod)

  return(evolution_ressources)
  }

gen_evol_parametres <- function() {
  #' gen_evol_parametres
  #'
  #' A partir des paramètres de législation observés et de projections d'inflation
  #' et d'évolution du smtp, projette l'évolution des paramètres de législation.
  #'
  #' @export
  #'
  #' @importFrom dplyr select
  #' @importFrom stringr str_extract
  #'
  # annee_depart est la première année à partir de laquelle les résultats sont projectifs

  parametres_sociaux_fiscaux <- ATNMXOutils::parametres_sociaux_fiscaux
  evolution_taux <- ATNMXOutils::evolution_taux

  derniere_annee_parametres <- max(as.integer(str_extract(colnames(parametres_sociaux_fiscaux), '[0-9]+$')), na.rm = T)
  annee_depart <- derniere_annee_parametres + 1

  annees <- c(annee_depart:2050)
  for (i in seq_len(length(annees))){
    an <- annees[i]
    an_1 <- an - 1
    prec_varname <- paste0("an_", an_1)
    varname <- paste0("an_", an)

    parametres_sociaux_fiscaux[parametres_sociaux_fiscaux$taux == 'manquant', varname] <- NA
    parametres_sociaux_fiscaux[parametres_sociaux_fiscaux$taux == 'mixte', varname] <-
      parametres_sociaux_fiscaux[parametres_sociaux_fiscaux$taux == 'mixte', prec_varname] *
      (1 + evolution_taux$mixte[evolution_taux$date == an])
    parametres_sociaux_fiscaux[parametres_sociaux_fiscaux$taux == 'smpt_reel', varname] <-
      parametres_sociaux_fiscaux[parametres_sociaux_fiscaux$taux == 'smpt_reel', prec_varname] *
      (1 + evolution_taux$smpt_reel[evolution_taux$date == an])
    parametres_sociaux_fiscaux[parametres_sociaux_fiscaux$taux == 'inflation', varname] <-
      parametres_sociaux_fiscaux[parametres_sociaux_fiscaux$taux == 'inflation', prec_varname] *
      (1 + evolution_taux$inflation[evolution_taux$date == an])
    parametres_sociaux_fiscaux[parametres_sociaux_fiscaux$taux == 'constant', varname] <-
      parametres_sociaux_fiscaux[parametres_sociaux_fiscaux$taux == 'constant', prec_varname] *
      (1 + evolution_taux$constant[evolution_taux$date == an])
  }

  dff <- parametres_sociaux_fiscaux %>% select(-c(parametre, nom, taux))

  dff <- data.frame(t(dff))
  colnames(dff) <- parametres_sociaux_fiscaux$parametre
  dff$ANNEE <- c(2015:2050)

  return(dff)

}

gen_calage_livia <- function(
  df_livia,
  scenario_ouverture_places_ehpad = 1,
  scenario_esperance_vie = "central",
  hypothese_evolution_dependance = "intermediaire"
  ){
  #' gen_calage_livia
  #'
  #' En fonction de trois scénarios d'ouverture de places en ehpad, génère les
  #' effectifs des personnes âgées en fonction de leur lieu de vie (ehpad,
  #' résidence autonomie ou domicile) et de leur niveau de dépendance.
  #' Permet de moduler le niveau de la population des seniors en fonction de
  #' scénarios d'espérance de vie et d'évolution de la dépendance.
  #'
  #' Calcule, par sexe et pour toute la population, les effectifs qui seront
  #' utilisés comme marge de calage dans Autonomix
  #'
  #' @param df_livia data.frame, contient les projections nationales du modèle Livia
  #' @param scenario_ouverture_places_ehpad integer parmi 1, 2, 3 (cf. documentation de Livia)
  #' @param scenario_esperance_vie character parmi "central", "edv_haute", "edv_basse". Défaut : "central"
  #' @param hypothese_evolution_dependance character parmi "intermediaire", "optimiste" et "pessimiste". Défaut : "intermediaire"
  #'
  #' @importFrom dplyr transmute

  if (scenario_ouverture_places_ehpad == 1) {
    df_livia <- df_livia %>% filter(scenario == scenario_esperance_vie,
                                    hypothese == hypothese_evolution_dependance) %>%
      select(ANNEE, SEXE, moins_75, vol_APA_m_DOM, vol_APA_s_DOM,
             vol_m_RA, vol_s_RA, vol_RA,
             vol_APA_m_ETAB, vol_APA_s_ETAB, vol_INS)
  }

  if (scenario_ouverture_places_ehpad == 2) {

    df_livia <- df_livia %>%
      filter(scenario == scenario_esperance_vie, hypothese == hypothese_evolution_dependance) %>%
      transmute(ANNEE = ANNEE,
                SEXE = SEXE,
                moins_75 = moins_75,
                vol_APA_m_DOM = vol_APA_m_DOM2C,
                vol_APA_s_DOM = vol_APA_s_DOM2C,
                vol_m_RA = vol_m_RA2C,
                vol_s_RA = vol_s_RA2C,
                vol_RA = vol_RA2C,
                vol_APA_m_ETAB = vol_APA_m_ETAB2C,
                vol_APA_s_ETAB = vol_APA_s_ETAB2C,
                vol_INS = vol_INS2C)
  }

  if (scenario_ouverture_places_ehpad == 3) {

    df_livia <- df_livia %>%
      filter(scenario == scenario_esperance_vie, hypothese == hypothese_evolution_dependance) %>%
      transmute(ANNEE = ANNEE,
                SEXE = SEXE,
                moins_75 = moins_75,
                vol_APA_m_DOM = vol_APA_m_DOM2P,
                vol_APA_s_DOM = vol_APA_s_DOM2P,
                vol_m_RA = vol_m_RA2P,
                vol_s_RA = vol_s_RA2P,
                vol_RA = vol_RA2P,
                vol_APA_m_ETAB = vol_APA_m_ETAB2P,
                vol_APA_s_ETAB = vol_APA_s_ETAB2P,
                vol_INS = vol_INS2P)
  }

  # d : ménage ordinaire
  # l : logement foyer
  # e : établissement
  df_ <- df_livia %>%
    filter(SEXE == "TOUS", moins_75 == "60 ans et plus")%>%
    mutate(total_apa_d = vol_APA_m_DOM + vol_APA_s_DOM - vol_s_RA - vol_m_RA, # hyp pas de non recours en logement foyer
           apa_d_gir12 = vol_APA_s_DOM - vol_s_RA,
           apa_d_gir34 = vol_APA_m_DOM - vol_m_RA,
           apa_d_gir56 = 0,
           total_apa_l = vol_RA,
           apa_l_gir12 = vol_s_RA,
           apa_l_gir34 = vol_m_RA,
           apa_l_gir56 = vol_RA - vol_s_RA - vol_m_RA,
           total_apa_e = vol_INS,
           apa_e_gir12 = vol_APA_s_ETAB,
           apa_e_gir34 = vol_APA_m_ETAB,
           apa_e_gir56 = vol_INS - vol_APA_m_ETAB - vol_APA_s_ETAB) %>%
    select(ANNEE, total_apa_d, apa_d_gir12, apa_d_gir34, apa_d_gir56,
           total_apa_l, apa_l_gir12, apa_l_gir34, apa_l_gir56,
           total_apa_e, apa_e_gir12, apa_e_gir34, apa_e_gir56)



  df_2 <- df_livia %>%
    filter(SEXE == "HOMMES", moins_75 == "60 ans et plus")%>%
    mutate(apa_d_homme = vol_APA_m_DOM + vol_APA_s_DOM - vol_s_RA - vol_m_RA,
           apa_l_homme = vol_RA,
           apa_l_homme_dep = vol_m_RA + vol_s_RA,
           apa_e_homme = vol_INS)%>%
    select(ANNEE, apa_d_homme, apa_l_homme, apa_e_homme, apa_l_homme_dep)

  df_ <- merge(df_, df_2, by=c("ANNEE"))

  df_2 <- df_livia %>%
    filter(SEXE == "FEMMES", moins_75 == "60 ans et plus")%>%
    mutate(apa_d_femme = vol_APA_m_DOM + vol_APA_s_DOM - vol_s_RA - vol_m_RA,
           apa_l_femme = vol_RA,
           apa_l_femme_dep = vol_m_RA + vol_s_RA,
           apa_e_femme = vol_INS)%>%
    select(ANNEE, apa_d_femme, apa_l_femme, apa_e_femme, apa_l_femme_dep)

  df_ <- merge(df_, df_2, by=c("ANNEE"))

  return(df_)
}

recal_aide_sociale <- function(df_livia) {
  #' recal_aide_sociale
  #'
  #' Pour les années pour lesquelles l'enquête aide sociale est disponible,
  #' remplace les marges générées avec le modèle Livia et les scénarios choisis,
  #' par l'observé pour les GIR 1-4. Les GIR 5-6 sont obtenus par différence.
  #'
  #' @param df_livia data.frame, contient les effectifs par sexe et par niveau de dépendance générés avec le modèle Livia
  #'
  #' @export
  #'
  #' @importFrom dplyr case_when mutate
  #' @importFrom data.table data.table

  df2 <- merge(df_livia, ATNMXOutils::effectifs_aide_sociale, by="ANNEE", all.x=TRUE)

  # établissement
  df2 <- df2 %>%
    mutate(apa_e_gir12 = case_when(!is.na(n_as_gir12_etab) ~ n_as_gir12_etab,
                                   TRUE ~ as.double(apa_e_gir12)),
           apa_e_gir34 = case_when(!is.na(n_as_gir34_etab) ~ n_as_gir34_etab,
                                   TRUE ~  as.double(apa_e_gir34)),
           apa_e_gir56 = case_when(!is.na(n_as_apa_etab) ~
                                     pmax(0, total_apa_e - n_as_gir34_etab -  n_as_gir12_etab),
                                   TRUE ~  as.double(apa_e_gir56)),
           apa_e_femme = case_when(!is.na(n_as_apa_etab) ~
                                     round(total_apa_e * n_as_femme_etab/n_as_apa_etab) ,
                                   TRUE ~  as.double(apa_e_femme)),
           apa_e_homme = case_when(!is.na(n_as_apa_etab) ~
                                     round(total_apa_e * n_as_homme_etab/n_as_apa_etab) ,
                                   TRUE ~  as.double(apa_e_homme)))


  # domicile
  df3 <- data.table(df2)
  df3 <- df3[, lapply(.SD, as.double)]
  df3 <- df3[ANNEE <= 2019, `:=`(c("apa_d_gir12", "apa_d_gir34", "apa_d_gir56"),
                                 .(round(n_as_gir12_dom * apa_d_gir12 / (apa_d_gir12 + apa_l_gir12)),
                                   round(n_as_gir34_dom * apa_d_gir34 / (apa_d_gir34 + apa_l_gir34)),
                                   0)
  )]

  df3 <- df3[ANNEE <= 2019, `:=`(c("total_apa_d"),
                                 .(apa_d_gir12 + apa_d_gir34)
  )]


  df3 <- df3[ANNEE <= 2019, `:=`(c("apa_d_femme", "apa_d_homme"),
                                 .(round(total_apa_d * n_as_femme_dom / n_as_apa_dom),
                                   round(total_apa_d * n_as_homme_dom / n_as_apa_dom))
  )]

  # résidence autonomie
  df3 <- df3[ANNEE <= 2019, `:=`(c("apa_l_gir12", "apa_l_gir34"),
                                 .(round(n_as_gir12_dom * apa_l_gir12 / (apa_d_gir12 + apa_l_gir12)),
                                   round(n_as_gir34_dom * apa_l_gir34 / (apa_d_gir34 + apa_l_gir34)))
  )]

  df3 <- df3[ANNEE <= 2019, `:=`(c("apa_l_gir56"),
                                 .(total_apa_l - apa_l_gir12 - apa_l_gir34)
  )]

  return(df3)

}

gen_param_historique <- function(dfr2) {
  #' gen_param_historique
  #'
  #' Réintègre les effectifs en résidence autonomie dans les effectifs de domicile, pour utilisation dans les deux volets historique d'Autonomix.
  #'
  #' @param dfr2 data.frame, contient les effectifs par sexe et par niveau de dépendance générés avec le modèle Livia et aide sociale
  #'
  #' @export
  #'
  #' @importFrom data.table :=

  dfr3 <- data.table(dfr2)
  dfr3 <- dfr3[, `:=`(c("total_apa_d", "apa_d_gir12", "apa_d_gir34", "apa_d_gir56",
                        "apa_d_femme", "apa_d_homme"),
                      .(total_apa_d + total_apa_l - apa_l_gir56,
                        apa_d_gir12 + apa_l_gir12,
                        apa_d_gir34 + apa_l_gir34,
                        apa_d_gir56 ,
                        apa_d_femme + round(apa_l_femme * (apa_l_gir12 + apa_l_gir34) / total_apa_l),
                        apa_d_homme + round(apa_l_homme * (apa_l_gir12 + apa_l_gir34) / total_apa_l))
  )]


  dfr3 <- dfr3[, `:=`(c("total_apa_l", "apa_l_gir12", "apa_l_gir34", "apa_l_gir56",
                        "apa_l_femme", "apa_l_homme"),
                      .(0, 0, 0, 0, 0, 0))]

  return(dfr3)
}


generation_parametres <- function(scenario_ouverture_places_ehpad = 1,
                                  scenario_esperance_vie = "central",
                                  hypothese_evolution_dependance = "intermediaire",
                                  annee_reference = 2016,
                                  mode_effectifs = "recales aide sociale" # ou "livia uniquement"
                                  ) {

  #' generation_parametres
  #'
  #' Permet de générer les paramètres sociaux-fiscaux ainsi que les marges de calage nécessaires
  #' pour faire tourner le modèle Autonomix de l'année de départ de la source utilisée à 2050, selon
  #' les options spécifiées.
  #'
  #' @param scenario_ouverture_places_ehpad integer parmi 1, 2, 3 (cf. documentation de Livia)
  #' @param scenario_esperance_vie character parmi "central", "edv_haute", "edv_basse". Défaut : "central"
  #' @param hypothese_evolution_dependance character parmi "intermediaire", "optimiste" et "pessimiste". Défaut : "intermediaire"
  #' @param annee_reference integer, année de référence des revenus. 2014 pour CARE-M (enquête 2015 sur revenus 2014), 2016 pour Care-I (enquête 2016 sur revenus 2016) et pour les RI (données 2017 sur revenus 2016)
  #' @param mode_effectifs character parmi "recales aide sociale" ou "livia uniquement" : "livia uniquement" correspond à des effectifs de calages qui viennent entièrement du modèle livia,
  #' "recales aide sociale" correspond à l'utilisation des effectifs d'aide sociale pour les années observées
  #'
  #' @export
  #'
  #' @importFrom data.table fread

  mode_historique <- TRUE

  # Evolution des ressources
  evol_ressources <- gen_evol_ressources(annee_reference)
  # Evolution des paramètres socio-fiscaux
  evolution_ressources_params <- gen_evol_parametres()

  dff <- merge(evolution_ressources_params, evol_ressources, by.x=c("ANNEE"), by.y=c("annee"), all.y = T)

  # calage livia, choix du scénario d'évolution du nb de places en Ehpad
  df_calage <- gen_calage_livia(ATNMXOutils::livia_national, scenario_ouverture_places_ehpad,
                                scenario_esperance_vie,
                                hypothese_evolution_dependance)

  # fichier de paramètres full Livia
  res <- merge(dff, df_calage, by=c("ANNEE"), all.x=TRUE)

  if(mode_effectifs == "recales aide sociale") {
    # fichier de paramètres livia recalé
    df_livia_recal <- recal_aide_sociale(df_calage)
    res <- merge(dff, df_livia_recal, by="ANNEE", all.x = TRUE)
  }

  if(mode_historique) {
    # fichiers de paramètres livia recalé avec uniquement Etablissement et Domicile, pas de résidence autonomie
    res <- gen_param_historique(res)
  }

  return(res)

}
