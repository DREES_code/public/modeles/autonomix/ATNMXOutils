# Package Autonomix Outils : les fonctions communes du modèle Autonomix

Ce dossier fournit les programmes permettant de former le package contenant les fonctions mobilisées par le modèle de microsimulation Autonomix version 1 de décembre 2021.

Lien vers la documentation du modèle : https://drees_code.gitlab.io/public/modeles/autonomix/DocumentationATNMX/

Présentation de la Drees : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères. 
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : Ce package utilise les données de l'enquête Aide Sociale (2015 à 2019) et du modèle de projection démographique Livia, dans leurs dernières versions disponibles au 1er décembre 2021. Traitements : Drees.

Date de la dernière exécution des programmes avant publication, et version des logiciels utilisés : Les programmes ont été exécutés le 14/12/2021 avec la version 4.0.5 de R. Les packages mobilisés sont listés dans le fichier DESCRIPTION du package. 
